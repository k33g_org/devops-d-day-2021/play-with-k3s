#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo --region=${CLUSTER_REGION} kubernetes show ${CLUSTER_NAME} > ./config/infos.txt
cat ./config/infos.txt

# External IP
EXTERNAL_IP=$(sed '9q;d' ./config/infos.txt)
# DNS A record
DNS=$(sed '10q;d' ./config/infos.txt)

echo "${EXTERNAL_IP#*:}" | xargs > ./config/external_ip.txt
echo "${DNS#*:}" | xargs > ./config/dns.txt

